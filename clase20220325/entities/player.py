import pygame
from pygame.locals import *

from config import ACELERATION, FRICTION, WIDTH


class Player(pygame.sprite.Sprite):
    def __init__(self, game) -> None:
        super().__init__()

        self.game = game
        self.surface = pygame.Surface((20, 60))
        self.surface.fill((0, 255, 255))

        # Aca iria la animacion proxima clase.

        self.jumping = False
        self.rect = self.surface.get_rect()
        self.position =pygame.math.Vector2((0, 0))
        self.velocity = pygame.math.Vector2((0, 0))
        self.aceleration = pygame.math.Vector2((0, 0))

    def move(self):
        self.aceleration = pygame.math.Vector2((0, 0.5))

        keys = pygame.key.get_pressed()
        if keys[K_LEFT]:
            self.aceleration.x = -ACELERATION
        elif keys[K_RIGHT]:
            self.aceleration.x = ACELERATION

        self.aceleration.x += self.velocity.x * FRICTION
        self.velocity += self.aceleration
        self.position += self.velocity + 0.5 * self.aceleration

        if self.position.x > WIDTH:
            self.position.x = 0
        if self.position.x < 0:
            self.position.x = WIDTH

        self.rect.midbottom = self.position

    def update(self):
        hits = pygame.sprite.spritecollide(self, self.game.platforms, False)
        if self.velocity.y > 0:
            if hits:
                if self.position.y < hits[0].rect.bottom:
                    self.velocity.y = 0
                    self.position.y = hits[0].rect.top + 1

    def jump(self):
        pass
