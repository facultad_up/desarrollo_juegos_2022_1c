import pygame


class Platform(pygame.sprite.Sprite):
    def __init__(self, width, height=15, background_color=(0, 0, 255)) -> None:
        super().__init__()
        self.surface = pygame.Surface((width, height))
        self.surface.fill(background_color)
        self.rect = self.surface.get_rect()

    # TASK: Hacer la plataforma movible
