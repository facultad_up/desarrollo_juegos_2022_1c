import pygame
from pygame.locals import *

from config import FPS, WIDTH, HEIGHT
from entities.platform import Platform
from entities.player import Player


class Game():
    def __init__(self) -> None:
        pygame.init()

        pygame.display.set_caption('My First Platform Game')
        self.clock = pygame.time.Clock()
        self.display_surface = pygame.display.set_mode((WIDTH, HEIGHT))
        self.display_surface.fill((217, 235, 240))
        self.platforms = pygame.sprite.Group()

        self._build_platforms()

        self.player = Player(self)
        self.player.rect.bottom = HEIGHT / 2
        self.player.position.x = 0
        self.player.position.y = self.player.rect.top


    def _build_platforms(self, quantity=3):
        # Task: Que funcione el quantity, ojo la superposicion.
        platform_a = Platform(WIDTH)
        platform_b = Platform(WIDTH/4)
        platform_c = Platform(WIDTH/3, 40)

        self.platforms.add(platform_a)
        self.platforms.add(platform_b)
        self.platforms.add(platform_c)

        platform_a.rect.bottom = HEIGHT
        
        platform_b.rect.top = HEIGHT / 2
        platform_b.rect.left = WIDTH / 2

        platform_c.rect.top = HEIGHT / 5
        platform_c.rect.right = WIDTH


    def start(self):
        in_game = True
        while in_game:

            # Manejo de los eventos
            for event in pygame.event.get():
                if event.type == QUIT:
                    in_game = False

            # Update
            self.player.move()
            self.player.update()

            # Draw
            self.display_surface.fill((217, 235, 240))
            for p in self.platforms:
                self.display_surface.blit(p.surface, p.rect)

            self.display_surface.blit(self.player.surface, self.player.rect)


            # Control de FPS
            pygame.display.update()
            self.clock.tick(FPS)
                

