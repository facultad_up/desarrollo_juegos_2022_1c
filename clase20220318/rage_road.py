import random
import time
import pygame

from pygame.locals import *

pygame.init()

# Inicializacion
RED = (255, 0, 0)

FPS = 60
clock = pygame.time.Clock()

main_surface = pygame.display.set_mode((400, 600))
pygame.display.set_caption('The rage road game pro')

font = pygame.font.SysFont('Verdana', 40)



# Assets
background = pygame.image.load('road.png')
player_image = pygame.image.load('Player.png')
enemy_image = pygame.image.load('Enemy.png')

# Entidades que se usan en el juego
class Player(pygame.sprite.Sprite):
    def __init__(self, image, game) -> None:
        super().__init__()
        self.game = game
        self.image = image
        self.surface = pygame.Surface((48, 96))
        self.rect = self.surface.get_rect(center=(208, 500))

    def update(self):
        keys = pygame.key.get_pressed()
        if self.rect.left > 60:
            if keys[K_LEFT]:
                self.rect.move_ip(-5, 0)

        if self.rect.right < 350:
            if keys[K_RIGHT]:
                self.rect.move_ip(5, 0)

class Enemy(pygame.sprite.Sprite):
    def __init__(self, image, game) -> None:
        super().__init__()
        self.game = game
        self.image = image
        self.surface = pygame.Surface((48, 93))
        self.posiciones = [60+24, 180+24, 295+24]
        pos_x = self.posiciones[random.randrange(0, len(self.posiciones))]
        self.rect = self.surface.get_rect(center=(pos_x, 0))

    def update(self):
        self.rect.move_ip(0, self.game.game_speed)
        if self.rect.top > 600:
            self.rect.top = 0
            pos_x = self.posiciones[random.randrange(0, len(self.posiciones))]
            self.rect = self.surface.get_rect(center=(pos_x, 0))
            self.game.score += 1
            if self.game.score % 5 == 0:
                self.game.game_speed += 2

class Game():
    score = 0
    game_speed = 5

    def get_score_sprite(self):
        return font.render('Score: ' + str(self.score), True, RED)

# El ciclo de vida del juego

game = Game()
enemy = Enemy(enemy_image, game)
player = Player(player_image, game)

sprites = pygame.sprite.Group()
sprites.add(enemy)
sprites.add(player)

enemies = pygame.sprite.Group()
enemies.add(enemy)

in_game = True
while in_game:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            in_game = False

    # Draw
    main_surface.blit(background, (0, 0))

    for sprite in sprites:
        main_surface.blit(sprite.image, sprite.rect)
    
    main_surface.blit(game.get_score_sprite(), (50, 5))

    # Update
    for sprite in sprites:
        sprite.update()

    if pygame.sprite.spritecollideany(player, enemies):
        main_surface.fill(RED)
        pygame.display.update()
        time.sleep(2)
        pygame.quit()
        break

    pygame.display.update()
    
    clock.tick(FPS)

